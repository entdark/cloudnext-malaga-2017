#!/usr/bin/env python

# [START imports]
import os
import urllib

from google.appengine.api import users
from google.appengine.ext import ndb

import jinja2
import webapp2

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)),
    extensions=['jinja2.ext.autoescape'],
    autoescape=True)
# [END imports]

DEFAULT_musicrating_NAME = 'default_guestbook'


# We set a parent key on the 'Greetings' to ensure that they are all
# in the same entity group. Queries across the single entity group
# will be consistent. However, the write rate should be limited to
# ~1/second.

def musicrating_key(musicrating_name=DEFAULT_musicrating_NAME):
    """Constructs a Datastore key for a MusicRating entity.
    We use musicrating_name as the key.
    """
    return ndb.Key('MusicRating', musicrating_name)


# [START greeting]
class Autor(ndb.Model):
    """Sub model for representing an Autor."""
    identity = ndb.StringProperty(indexed=False)
    email = ndb.StringProperty(indexed=False)


class Comentario(ndb.Model):
    """A main model for representing an individual MusicRating entry."""
    Autor = ndb.StructuredProperty(Autor)
    content = ndb.StringProperty(indexed=False)
    date = ndb.DateTimeProperty(auto_now_add=True)
# [END greeting]


# [START main_page]
class MainPage(webapp2.RequestHandler):

    def get(self):
        musicrating_name = self.request.get('musicrating_name',
                                          DEFAULT_musicrating_NAME)
        comentarios_query = Comentario.query(
            ancestor=musicrating_key(musicrating_name)).order(-Comentario.date)
        comentarios = comentarios_query.fetch(10)

        user = users.get_current_user()
        if user:
            url = users.create_logout_url(self.request.uri)
            url_linktext = 'Logout'
        else:
            url = users.create_login_url(self.request.uri)
            url_linktext = 'Login'

        template_values = {
            'user': user,
            'comentarios': comentarios,
            'musicrating_name': urllib.quote_plus(musicrating_name),
            'url': url,
            'url_linktext': url_linktext,
        }

        template = JINJA_ENVIRONMENT.get_template('index.html')
        self.response.write(template.render(template_values))
# [END main_page]


# [START MusicRating]
class MusicRating(webapp2.RequestHandler):

    def post(self):
        # We set the same parent key on the 'Greeting' to ensure each
        # Greeting is in the same entity group. Queries across the
        # single entity group will be consistent. However, the write
        # rate to a single entity group should be limited to
        # ~1/second.
        musicrating_name = self.request.get('musicrating_name',
                                          DEFAULT_musicrating_NAME)
        comentario = Comentario(parent=musicrating_key(musicrating_name))

        if users.get_current_user():
            comentario.Autor = Autor(
                    identity=users.get_current_user().user_id(),
                    email=users.get_current_user().email())

        comentario.content = self.request.get('content')
        comentario.put()

        query_params = {'musicrating_name': musicrating_name}
        self.redirect('/?' + urllib.urlencode(query_params))
# [END MusicRating]


# [START app]
app = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/rate', MusicRating),
], debug=True)
